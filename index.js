//https://www.codingame.com/ide/puzzle/temperatures

const n = parseInt(readline()); // the number of temperatures to analyse
var inputs = readline().split(' ');
var closestToZero = parseInt(inputs[0]);

if (!inputs.length || Number.isNaN(closestToZero)) { closestToZero = 0; }

for (let i = 0; i < n; i++) {
    const t = parseInt(inputs[i]);
    if ( Math.abs(closestToZero) === Math.abs(t) && closestToZero < 0
        || Math.abs(closestToZero) > Math.abs(t) ) {
        closestToZero = t;
    }
}
console.log(closestToZero);
